export type CreateUserReqBody = {
  name: string;
  email: string;
  password: string;
};

export type LoginUserReqBody = {
  email: string;
  password: string;
};
