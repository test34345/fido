import { Dispatch, SetStateAction } from "react";

export type AuthContextValue = {
  isLoggedIn: boolean;
  isLoggedInChanged: boolean;
  authMessage: string;
};

export type AuthContextValueExposed = {
  value: AuthContextValue;
  setValue: Dispatch<SetStateAction<AuthContextValue>>;
  logout: () => void;
};

export type AuthContextProviderProps = {
  children: React.ReactNode;
};
