import { UserInContext } from "../user";

export type UserContextValue = {
  user: UserInContext | null;
  setUser: React.Dispatch<React.SetStateAction<UserInContext>>;
};

export type UserContextProviderProps = {
  children: React.ReactNode;
};
