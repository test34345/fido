import { startRegistration } from "@simplewebauthn/browser";
import { User } from "../models/user";
import {
  AttestationResponse,
  RegistrationVerificationReqBody,
} from "../models/webauthn";
import { AuthDataFetch } from "./fetchData";
import { AuthenticationCancelledError } from "./Errors/authCancelled";
import { ErrorType } from "../models/general";
import { AuthenticatorAlreadyRegistered } from "./Errors/authenticatorAlreadyRegistered";

export class WebAuthnRegister {
  private static async generateRegistrationOptions(userData: User) {
    const registrationOptions = await AuthDataFetch.generateOptions({
      optionsType: "registration",
      data: userData,
    });

    return registrationOptions as AttestationResponse;
  }

  private static async startRegistration(userData: User) {
    const registrationOptionsResponse = await this.generateRegistrationOptions(
      userData
    );

    console.log({ registrationOptionsResponse });

    try {
      const attestationResp = await startRegistration(
        registrationOptionsResponse.generatedOptions
      );

      console.log({ attestationResp });

      return {
        credentials: attestationResp as any,
        user: registrationOptionsResponse.user,
      };
    } catch (err) {
      console.log((err as ErrorType).message);
      throw new AuthenticationCancelledError();
    }
  }

  private static async verifyRegistration(
    verificationData: RegistrationVerificationReqBody
  ) {
    try {
      const resp = await AuthDataFetch.verifyRegistration(verificationData);
      return resp.data;
    } catch (err) {
      console.log(err);
    }
  }

  static async registerUser(userData: User) {
    const attestationResp = await this.startRegistration(userData);
    const verificationData = await WebAuthnRegister.verifyRegistration({
      credentials: attestationResp.credentials,
      user: attestationResp.user,
    });

    return verificationData;
  }
}
