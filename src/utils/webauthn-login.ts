import { startAuthentication } from "@simplewebauthn/browser";
import {
  AuthLoginVerificationResponse,
  GenerateAuthenticationOptionsResponse,
  StartAuthenticationConfig,
} from "../models/webauthn";
import { AuthDataFetch } from "./fetchData";
import { AuthenticationCancelledError } from "./Errors/authCancelled";
import { LoginError } from "./Errors/loginError";
import { AxiosError } from "axios";
import { AuthenticatorNotRegisteredError } from "./Errors/authenticatorNotRegistered";

export class WebAuthnLogin {
  private static async generateAuthOptions(email: string) {
    try {
      const authOptions =
        await AuthDataFetch.generateOptions<GenerateAuthenticationOptionsResponse>(
          {
            optionsType: "auth",
            data: { email },
          }
        );

      return authOptions;
    } catch (err) {
      throw new LoginError();
    }
  }

  private static async startAuthentication(
    authOptionsConfig: StartAuthenticationConfig
  ): Promise<AuthLoginVerificationResponse> {
    const { authOptions, user } = authOptionsConfig;

    try {
      const authnResp = await startAuthentication(authOptions);

      const verificationResponse =
        await AuthDataFetch.verifyLoginAuthentication({
          credentials: authnResp,
          user,
        });

      return verificationResponse;
    } catch (err) {
      if (
        err instanceof AxiosError &&
        err.response?.data.message === "Authenticator not found"
      ) {
        throw new AuthenticatorNotRegisteredError();
      }

      throw new AuthenticationCancelledError();
    }
  }

  static async loginUser(email: string) {
    const autOptionsResponse = await this.generateAuthOptions(email);

    if (!autOptionsResponse) throw new LoginError();

    const isVerified = await this.startAuthentication(autOptionsResponse);
    return isVerified;
  }
}
