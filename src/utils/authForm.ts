import { AuthFormMode } from "../models/general";

export const getOppositeAuthMode = (authMode: AuthFormMode): AuthFormMode =>
  authMode === "login" ? "register" : "login";
