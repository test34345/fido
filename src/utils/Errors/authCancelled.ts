export class AuthenticationCancelledError extends Error {
  constructor() {
    super();
    this.message = "Authentication cancelled";
  }
}
