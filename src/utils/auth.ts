import { GetUserAuthActionHandler } from "../models/auth";
import { UserDataFetch } from "./fetchUserData";

export const getUserAuthAction: GetUserAuthActionHandler = (form, authMode) => {
  if (authMode === "login") {
    return UserDataFetch.loginUser({
      email: form.email,
      password: form.password,
    });
  }

  return UserDataFetch.createUser({
    name: form.name,
    email: form.email,
    password: form.password,
  });
};
