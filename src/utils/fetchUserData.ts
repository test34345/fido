import axios from "axios";
import { CreateUserReqBody, LoginUserReqBody } from "../models/user/fetch";
import { CreatedUserResponse, LoginUserResponse } from "../models/user";
import { UserAlreadyExistsError } from "./Errors/userExists";
import { UserDoesNotExistError } from "./Errors/userDoesNotExist";
import { API } from "./constants";

export abstract class UserDataFetch {
  static async createUser(reqBody: CreateUserReqBody) {
    try {
      const endpoint = `${API.url.dev}/api/user/create`;
      const response = await axios.post<CreatedUserResponse>(endpoint, reqBody);
      return response.data;
    } catch (err) {
      throw new UserAlreadyExistsError();
    }
  }

  static async loginUser(reqBody: LoginUserReqBody) {
    try {
      const endpoint = `${API.url.dev}/api/user/login`;
      const response = await axios.post<LoginUserResponse>(endpoint, reqBody);
      return response.data;
    } catch (err) {
      throw new UserDoesNotExistError();
    }
  }
}
