import { useContext } from "react";
import "./Header.scss";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../../context/userContext";
import { initialUserData } from "../../context/userContext/initialValues";

const Header: React.FC = () => {
  const { user, setUser } = useContext(UserContext);
  const navigateTo = useNavigate();

  const logoutHandler = () => {
    setUser(initialUserData);
    navigateTo("/");
  };

  return (
    <header className="header">
      <h1 className="header__logo">
        <Link to="/">WebAuthn</Link>
      </h1>

      {user?.isLoggedIn && (
        <div className="header__auth-box">
          <button type="button" onClick={logoutHandler}>
            Log out
          </button>
        </div>
      )}
    </header>
  );
};

export default Header;
