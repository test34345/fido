import { useContext } from "react";
import useRedirect from "../../hooks/useRedirect";
import { AuthContext } from "../../context/authContext";

const AuthFailurePage = () => {
  const { value: authContextValue } = useContext(AuthContext);

  useRedirect();

  return (
    <section style={{ textAlign: "center", marginTop: "30px" }}>
      <h2 style={{ color: "red" }}>You have failed to authenticate!</h2>

      <p style={{ fontSize: "1.4rem" }}>{authContextValue.authMessage}</p>
    </section>
  );
};

export default AuthFailurePage;
