import { useContext } from "react";
import useRedirect from "../../hooks/useRedirect";
import { AuthContext } from "../../context/authContext";

const AuthSuccessPage: React.FC = () => {
  const { value: authContextValue } = useContext(AuthContext);

  useRedirect();

  return (
    <section style={{ textAlign: "center", marginTop: "30px" }}>
      <h2 style={{ color: "green" }}>
        You have been successfully authenticated!
      </h2>

      <p style={{ fontSize: "1.4rem" }}>{authContextValue.authMessage}</p>
    </section>
  );
};

export default AuthSuccessPage;
