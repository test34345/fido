import { useContext } from "react";
import { UserContext } from "../../context/userContext";
import { Navigate, useNavigate } from "react-router-dom";
import "./UserDetailsPage.scss";
import UserAuthenticators from "../../components/user/UserAuthenticators/UserAuthenticators";
import { AuthDataFetch } from "../../utils/fetchData";
import LoadingSpinner from "../../components/general/LoadingSpinner/LoadingSpinner";
import { ErrorType } from "../../models/general";
import useUserDetails from "../../hooks/useUserDetails";
import { initialUserData } from "../../context/userContext/initialValues";

const UserDetailsPage = () => {
  const { user, setUser } = useContext(UserContext);
  const {
    value: userDetailsValue,
    resetLoaderAndUserDetailsData,
    setUserDetails,
    isDeletingUser,
  } = useUserDetails();
  const navigateTo = useNavigate();

  if (!user) {
    return <Navigate to="/" />;
  }

  const deleteUserHandler = async () => {
    setUserDetails((prevValue) => ({
      ...prevValue,
      action: "delete-user",
      isRequestLoading: true,
    }));

    try {
      await AuthDataFetch.deleteUser(user.id);
      setUser({
        ...initialUserData,
        isLoggedIn: false,
      });
      navigateTo("/");
    } catch (err) {
      const error = err as ErrorType;

      alert(error.message);
      setUserDetails((prevValue) => ({
        ...prevValue,
        errorMessage: (err as ErrorType).message,
      }));
    } finally {
      resetLoaderAndUserDetailsData();
    }
  };

  return (
    <>
      {!user && <Navigate to="/" />}
      <section className="user-details">
        <div className="content">
          <div className="user-details__data">
            <p>Name: {user.userName}</p>
            <p>Email: {user.email}</p>

            {isDeletingUser && <LoadingSpinner />}

            <button
              type="button"
              onClick={deleteUserHandler}
              disabled={userDetailsValue.isRequestLoading}
            >
              Delete User
            </button>
          </div>

          <div className="user-details__authenticators">
            <UserAuthenticators />
          </div>
        </div>
      </section>
    </>
  );
};

export default UserDetailsPage;
