/* eslint-disable @typescript-eslint/no-empty-function */
import {
  AuthContextValue,
  AuthContextValueExposed,
} from "../../models/store/authContext";

export const initialContextValue: AuthContextValue = {
  isLoggedIn: false,
  isLoggedInChanged: false,
  authMessage: "",
};

export const initialExposedValue: AuthContextValueExposed = {
  value: initialContextValue,
  setValue: () => {},
  logout: () => {},
};
