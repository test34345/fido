/* eslint-disable @typescript-eslint/no-empty-function */
import { UserContextValue } from "../../models/store/userContext";
import { UserInContext } from "../../models/user";

export const initialUserData: UserInContext = {
  _id: "",
  userName: "",
  email: "",
  password: "",
  challenge: "",
  loginChallenge: "",
  isLoggedIn: false,
  authenticators: [],
};

export const userContextInitialValue: UserContextValue = {
  user: initialUserData,
  setUser: () => {},
};
